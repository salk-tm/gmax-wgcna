# _G. max_ WGCNA gene network analysis

This repo describes a network analysis of 52776 _G. max_ genes across 71 samples. It uses [WGCNA](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/), specifically a modified version of a pipeline from [Yu et al.](https://github.com/PengYuMaize/Yu2021NaturePlants). The size of this dataset [pushes the limits](https://peterlangfelder.com/2018/11/25/blockwise-network-analysis-of-large-data/) of standard WGCNA analysis, but luckily we have computing equipment with sufficient memory to handle it.

## Setup

To run the analysis, first set up and activate a conda environment with the
required R packages
```sh
conda create -c conda-forge -c bioconda -n gmax-wgcna r-base r-essentials \
  r-devtools r-docopt r-wgcna bioconductor-deseq2
conda activate gmax-wgcna
```

Clone and navigate to the repository:

```sh
git clone https://gitlab.com/salk-tm/gmax-wgcna.git
cd gmax-wgcna
```

## Execution

The R script `Gmax_WGCNA.R` contains the pipeline. It has a few command-line options to facilitate testing before running the full pipeline. To see them, run:

```sh
Rscript Gmax_WGCNA.R --help
```
```
Gmax WGCNA gene network analysis.

Usage:
  WGCNA.Rscript.R [--block-size=<int>] [--mes-only] [--setwd=<wd>] [--subset]
  WGCNA.Rscript.R --help

Options:
  -h --help           Show this screen.
  --block-size=<int>  Set the maximum block size [default: 5000].
  --mes-only          Calculate MEs, then stop.
  --setwd=<wd>        Set the working directory.
  --subset            Run on a subset of the data. 
```

For a quick test, run with the `--mes-only` and `--subset` options, to just generate `newMEs.txt` for a subset of the data:

```sh
Rscript Gmax_WGCNA.R --mes-only --subset
```

If running interactively instead of by command line, you can change the parameters on lines 24-29 of the script. For example:

```r
# If running interactively instead of by command line, the parameters can be
# set here
max_block_size = args[["block_size"]] # Integer, default 5000
MEs_only = TRUE # TRUE or FALSE
working_directory = args[["setwd"]] # char, path to working directory, default NULL
subset = TRUE # TRUE or FALSE
```


To run the full pipeline on subsetted data:

```sh
Rscript Gmax_WGCNA.R --subset
```

Interactively:

```r
# If running interactively instead of by command line, the parameters can be
# set here
max_block_size = args[["block_size"]] # Integer, default 5000
MEs_only = args[["mes_only"]] # TRUE or FALSE
working_directory = args[["setwd"]] # char, path to working directory, default NULL
subset = TRUE # TRUE or FALSE
```


To run the full pipeline on the entire dataset you will need to increase the
block size to a value higher than the number of genes. In this case the command is:

```sh
Rscript Gmax_WGCNA.R --block-size=53000
```

Interactively:

```r
# If running interactively instead of by command line, the parameters can be
# set here
max_block_size = 53000 # Integer, default 5000
MEs_only = args[["mes_only"]] # TRUE or FALSE
working_directory = args[["setwd"]] # char, path to working directory, default NULL
subset = args[["subset"]] # TRUE or FALSE
```

## Results

Several output files will be generated. When the `--mes-only` option is used:

```
1-n-sampleClustering.pdf
2-n-sft.pdf
4-module_tree_blockwise.pdf
GmaxTOM-block.1.RData
newMEs.txt
```

When `--mes-only` is not used, you will get the above and additionally:
```
output_for_cytoscape/
```

Cytoscape output of the full pipeline run can be downloaded from:

https://salk-tm-pub.s3.us-west-2.amazonaws.com/Gmax_WGCNA/output_for_cytoscape.zip
